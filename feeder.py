#! /usr/bin/python3

import feedparser
import json
import os, sys
import requests
import hashlib
import pymsteams
import time
import copy

cwd = os.path.dirname(os.path.realpath(__file__)) + "/"
feed_list_file = "feed_list.json"
headers = {"Content-Type": "application/json"}
webhook_file="webhook_url.json"
webhook_url_null = "null"
webhook_urls= json.load(open(os.path.join(cwd+webhook_file), "r+"))
last_feed_fname = "last_feed.json"
last_feed_fname_backup = "last_feed_backup.json"
max_message_size = 15500 # ~15KB with some buffer for fiting in for each entry message 

def file_json_prepare(file_path, backup_file_path):
    """ Check if file is empty and create json file if needed"""
    if os.path.exists(file_path) and os.stat(file_path).st_size == 0:
        file_ = open(os.path.join(file_path), "r+")
        if os.path.exists(backup_file_path) and os.stat(backup_file_path).st_size != 0:
            backup_file = open(os.path.join(backup_file_path), "r")
            for x in backup_file.readlines():
                file_.write(x)
            backup_file.close()
        else:
            file_.write('{}')
        file_.close()

def create_card_section(entry_):
    Section = pymsteams.cardsection()
    Section.title(entry_.title if "title" in entry_ else "")
    if "link" in entry_:
        Section.linkButton("open in WEB", entry_.link)
    if "media_content" in entry_ and "url" in entry_.media_content[0]:
        Section.addImage(entry_.media_content[0]["url"])
    if "description" in entry_:
        if sys.getsizeof(entry_.description) > max_message_size:
            Section.text(entry_.description[:6000] + "...\n...MORE INFORMATION ON PAGE...\n")
        else:
            Section.text(entry_.description)
    elif "summary" in entry_:
         if sys.getsizeof(entry_.summary) > max_message_size:
            Section.text(entry_.summary[:6000] + "...\n...MORE INFORMATION ON PAGE...\n")
         else:
            Section.text(entry_.summary)
    else:
        Section.text("")

    return Section

def create_message(feed_channel_,webhook_url_):
     TeamsMessage = pymsteams.connectorcard(hookurl=webhook_url_, http_proxy="http://proxy-chain.intel.com:911")
     TeamsMessage.color("E81123")
     TeamsMessage.title(feed_channel_)
     TeamsMessage.summary(feed_channel_)
     return TeamsMessage

if __name__ == "__main__":

        file_json_prepare(cwd+last_feed_fname, cwd+last_feed_fname_backup)
        last_feed_data = json.load(open(os.path.join(cwd+last_feed_fname), "r+"))
        while True:
            with open(os.path.join(cwd+feed_list_file), 'r+') as feed_list_json:
                feed_list = json.load(feed_list_json)
                for feed_channel in feed_list:
                    print(feed_channel)
                    with open(os.path.join(cwd+last_feed_fname), "w") as last_feed_file:
                        url = feed_list[feed_channel]
                        try:
                            feed_content = feedparser.parse(url)
                        except Exception as ex:
                            print(str(ex))
                            time.sleep(5)
                            feed_content = None
                            continue
                        if feed_content is None:
                            continue
                        if not url in last_feed_data:
                            last_feed_data[url] = ""
                        myTeamsMessage = create_message(feed_channel, webhook_url_null)
                        send_message = 0
                        for entry in feed_content.entries:
                            feed_hash = str(hashlib.md5(str(entry.link + entry.title).encode('utf-8')).hexdigest())
                            if feed_hash == last_feed_data[url]:
                                break
                            else:
                                send_message = 1
                                message_section = create_card_section(entry)
                                message_sized = copy.deepcopy(myTeamsMessage)
                                message_sized.addSection(message_section)
                                # each send teams message for teams should be < 24 KB
                                if sys.getsizeof(json.dumps(message_sized.payload)) > max_message_size:
                                    for webhook in webhook_urls:
                                        myTeamsMessage.hookurl = webhook_urls[webhook]
                                        #myTeamsMessage.printme()
                                        try:
                                            myTeamsMessage.send()
                                        except pymsteams.TeamsWebhookException as ex:
                                            #print(str(ex))
                                            continue
                                        time.sleep(1)
                                    myTeamsMessage = create_message(feed_channel, webhook_url_null)

                                myTeamsMessage.addSection(message_section)

                        if send_message:
                            for webhook in webhook_urls:
                                myTeamsMessage.hookurl = webhook_urls[webhook]
                                #myTeamsMessage.printme()
                                try:
                                    myTeamsMessage.send()
                                except pymsteams.TeamsWebhookException as ex:
                                    #print(str(ex))
                                    continue
                                time.sleep(1)
                        try:
                            last_feed_data[url]=str(hashlib.md5(str(feed_content.entries[0].link + feed_content.entries[0].title).encode('utf-8')).hexdigest())
                        except Exception as ex:
                            print(str(ex))
                            continue
                        last_feed_file.write(json.dumps(last_feed_data))
                        with open(os.path.join(cwd+last_feed_fname_backup), "w") as feed_snap:
                                feed_snap.write(json.dumps(last_feed_data))
            time.sleep(15*60)

